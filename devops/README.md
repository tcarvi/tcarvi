### Execução local do projeto, usando docker
- **script startDocker**
    - `docker build -t naporteira:latest .`
    - `docker run --name tServer -p 8081:8081 naporteira:latest`
  
### Clean das imagens e containers docker
- **script cleanDocker**
    - `docker stop tServer`
    - `docker rm tServer`
    - `docker rmi tserverimage`
    - `docker system prune --force`